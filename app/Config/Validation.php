<?php

namespace Config;

use CodeIgniter\Validation\CreditCardRules;
use CodeIgniter\Validation\FileRules;
use CodeIgniter\Validation\FormatRules;
use CodeIgniter\Validation\Rules;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var string[]
	 */
	public $ruleSets = [
		Rules::class,
		FormatRules::class,
		FileRules::class,
		CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array<string, string>
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	public $regValPrivado = [
		'docid' => [
            'rules'  => 'required|exact_length[8]',
            'errors' => [
                'required' => 'El campo dni es obligatorio',
                'exact_length' => 'El DNI debe ser de 8 digitos'
              ]
            ],

            'nombre' => [
                'rules' => 'required|min_length[2]|max_length[30]|string',
                'errors' =>[
                    'required' => 'El campo nombre es obligatorio',
                    'min_length' => 'El nombre no debe ser menor de 2 caracteres',
                    'max_length' => 'El nombre no debe ser mayor de 30 caracteres',
                    'string' => 'El nombre solo son letras',
                    ]
                ]		,
                'apel' => [
                    'rules' => 'required|min_length[2]|max_length[50]|string',
                    'errors' =>[
                        'required' => 'El campo apellido es obligatorio',
                        'min_length' => 'El apellido no debe ser menor de 2 caracteres',
                        'max_length' => 'El apellido no debe ser mayor de 50 caracteres',
                        'string' => 'El apellido no debe tener caracteres no letras',
                        ]
                    ]		,
                  'cel' => [
              'rules' => 'required|min_length[8]|max_length[9]
|numeric',
              'errors' =>[
                  'required' => 'El campo numero celular es obligatorio',
                  'min_length' => 'El numero celular no debe ser menor de 8 caracteres',
                  'max_length' => 'El numero celular no debe ser mayor de 9 caracteres',
                  'numeric' => 'El numero celular no debe tener caracteres que no sean digitos'
                  ]
              ]	,
              'correo' => [
              'rules' => 'required|min_length[5]|max_length[30]
|valid_email',
              'errors' =>[
                  'required' => 'El campo correo eletronico es obligatorio',
                  'min_length' => 'El correo eletronico no debe ser menor de 5 caracteres',
                  'max_length' => 'El correo eletronico no debe ser mayor de 30 caracteres',
                  'valid_email' => 'El correo eletronico debe respetar el formato de un '
                  ]
              ]	

,
               
			   'det' => [
				'rules' => 'required|min_length[10]|max_length[400]|string',
				'errors' =>[
					'required' => 'El campo detalles de lo ocurrido es obligatorio',
					'min_length' => 'El campo detalles de lo ocurrido no debe ser menor de 10 caracteres',
					'max_length' => 'El campo detalles de lo ocurrido no debe ser mayor de 400 caracteres',
					'string' => 'El campo detalles de lo ocurrido debe respetar las reglas de puntuación'
					]
				]	

,
            'tip' => [
                'rules' => 'required|in_list[Sexual, Psicológico, Económico, Físico]',
                'errors' =>[
                    'required' => 'El campo tipo de maltrato es obligatorio',
                    'in_list' => 'El campo tipo de maltrato debe ser los datos de seleccion'
                    ]
                ]	
            


,
				'cant' => [
				'rules' => 'required|integer|less_than[31]|greater_than[0]',
				'errors' =>[
					'required' => 'El campo Cantidad de Agresores es obligatorio',
					'integer' => 'El campo Cantidad de Agresores debe ser los datos de seleccion',
					
				   'greater_than' => 'El campo Cantidad de Agresores no debe ser menor a 1 ',
					
					
					]
				]	

,
				   'fecha' => [
					   'rules'=> 'required',
					   'errors' => [
						   'required' => 'La fecha del incidente es obligatoria'
					   ]
				   ]
				   ,
                   'dir' => [
                    'rules' => 'required|min_length[10]|max_length[200]
                |string',
                    'errors' =>[
                        'required' => 'El campo direccion es obligatorio',
                        'min_length' => 'El campo direccion no debe ser menor de 10 caracteres',
                        'max_length' => 'El campo direccion no debe ser mayor de 200 caracteres',
                        'string' => 'El campo direccion debe respetar caracteres alfa numericos'
                        
                        ]
                    ]

                 ,

				   'foto' =>[
					   'uploaded[foto]',
					   'mime_in[foto,image/jpg,image/jpeg,image/png,application/pdf,application/octet-stream,video/mp4,audio/mpeg3,audio/mp3]',
					   'max_size[foto,1024]',
					   'errors' =>[
						   'uploaded' => 'No se envio un archivo',
						   'mime_in' => 'No se envio uun formato aceptado(Fotos: .jpg, .png, .jpeg
						   - Videos: .mp4, .avi
						   - Audio: .mp3
						   - Documento: .docx, .pdf)',
						   'max_size' => 'El archivo no debe exceder de 1MB'
					   ]
				   ]
	];

	public $regValAnonimo=[

		'dir1' => [
            'rules' => 'required|min_length[10]|max_length[100]
|string',
            'errors' =>[
                'required' => 'El campo direccion es obligatorio',
                'min_length' => 'El campo direccion no debe ser menor de 10 caracteres',
                'max_length' => 'El campo direccion no debe ser mayor de 100 caracteres',
                'string' => 'El campo direccion debe respetar caracteres alfa numericos'
                ]
            ]	
,	
		'det1' => [
				'rules' => 'required|min_length[10]|max_length[400]
|string',
				'errors' =>[
					'required' => 'El campo detalles de lo ocurrido es obligatorio',
					'min_length' => 'El campo detalles de lo ocurrido no debe ser menor de 10 caracteres',
					'max_length' => 'El campo detalles de lo ocurrido no debe ser mayor de 400 caracteres',
					'string' => 'El campo detalles de lo ocurrido debe respetar caracteres alfa numericos'
					]
				]	

,



            'tip1' => [
                'rules' => 'required|in_list[Psicologico,Sexual,Fisico,Economico]',
                'errors' =>[
                    'required' => 'El campo tipo de maltrato es obligatorio',
                    'in_list' => 'El campo tipo de maltrato debe ser los datos de seleccion'
                    ]
                ]	

,
            'cant1' => [
            'rules' => 'required|integer|less_than[31]|greater_than[1]',
            'errors' =>[
                'required' => 'El campo Cantidad de Agresores es obligatorio',
                'integer' => 'El campo Cantidad de Agresores debe ser los datos de seleccion',
                'less_than' => 'El campo Cantidad de Agresores no debe exceder las 30horas',
               'greater_than' => 'El campo Cantidad de Agresores no debe ser menor a 2 horas',
                
                
                ]
            ]	,

            'fecha1' => [
               'rules'=> 'required',
               'errors' => [
                   'required' => 'La fecha del incidente es obligatoria'
               ]
           ]
           ,

           'foto1' =>[
               'uploaded[foto]',
               'mime_in[foto,image/jpg,image/jpeg,image/png,application/pdf,application/octet-stream,video/mp4,audio/mpeg3,audio/mp3]',
               'max_size[foto,1024]',
               'errors' =>[
                   'uploaded' => 'No se envio un archivo',
                   'mime_in' => 'No se envio uun formato aceptado(Fotos: .jpg, .png, .jpeg
                   - Videos: .mp4, .avi
                   - Audio: .mp3
                   - Documento: .docx, .pdf)',
                   'max_size' => 'El archivo no debe exceder de 1MB'
               ]
           ]
]	;

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------
}
