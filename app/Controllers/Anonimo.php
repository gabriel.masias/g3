<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\AnonimoModelo;
class Anonimo extends BaseController
{

	public function __construct() {
     	  helper(['form', 'url']);
        
       	    	            	
    }
	public function index()
	{
		return view('header').view('Anonimo').view('footer');
	}
   

public function doSave()
	{
		$validation =  \Config\Services::validation();
		$respuesta = array();
          
    
      $input = $this->validate([
        'aliasP' => [
            'rules' => 'required|min_length[2]|max_length[30]|string',
            'errors' =>[
                'required' => 'El campo Alias es obligatorio',
                'min_length' => 'El nombre no debe ser menor de 2 caracteres',
                'max_length' => 'El nombre no debe ser mayor de 30 caracteres',
                'string' => 'El nombre debe respetar reglas de puntuación'
                ]
            ]	
            ,
        'dir' => [
            'rules' => 'required|min_length[10]|max_length[100]
|string',
            'errors' =>[
                'required' => 'El campo direccion es obligatorio',
                'min_length' => 'El campo direccion no debe ser menor de 10 caracteres',
                'max_length' => 'El campo direccion no debe ser mayor de 100 caracteres',
                'string' => 'El campo direccion debe respetar caracteres alfa numericos'
                ]
            ]	

,
                    'det' => [
                        'rules' => 'required|min_length[10]|max_length[200]
                    |string',
                        'errors' =>[
                            'required' => 'El campo direccion es obligatorio',
                            'min_length' => 'El campo direccion no debe ser menor de 10 caracteres',
                            'max_length' => 'El campo direccion no debe ser mayor de 200 caracteres',
                            'string' => 'El campo direccion debe respetar caracteres alfa numericos'
                            
                            ]
                        ]


,
            'tip' => [
                'rules' => 'required|in_list[Psicologico, Sexual, Fisico, Economico]',
                'errors' =>[
                    'required' => 'El campo tipo de maltrato es obligatorio',
                    'in_list' => 'El campo tipo de maltrato debe ser los datos de seleccion'
                    ]
                ]	

,
            'cant' => [
            'rules' => 'required|integer|less_than[31]|greater_than[0]',
            'errors' =>[
                'required' => 'El campo Cantidad de Agresores es obligatorio',
                'integer' => 'El campo Cantidad de Agresores debe ser los datos de seleccion',
                'less_than' => 'El campo Cantidad de Agresores no debe exceder las 30 personas',
               'greater_than' => 'El campo Cantidad de Agresores no debe ser menor a 1 persona',
                
                
                ]
            ]	,

            'fecha' => [
               'rules'=> 'required',
               'errors' => [
                   'required' => 'La fecha del incidente es obligatoria'
               ]
           ]
           ,

           'correo' => [
            'rules' => 'required|min_length[5]|max_length[30]
  |valid_email',
            'errors' =>[
                'required' => 'El campo correo eletronico es obligatorio',
                'min_length' => 'El correo eletronico no debe ser menor de 5 caracteres',
                'max_length' => 'El correo eletronico no debe ser mayor de 30 caracteres',
                'valid_email' => 'El correo eletronico debe respetar el formato de un '
                ]
            ]	




     ]);

       if (!$input) {
       	 $respuesta['error'] = $this->validator->listErrors() ;
  
        } else {
                $request =  \Config\Services::request();
               
              $aliasP= $request->getPostGet('aliasP') ;
              $det= $request->getPostGet('det') ;
              $tip= $request->getPostGet('tip') ; 
              $cant= $request->getPostGet('cant') ; 
              $fec= $request->getPostGet('fecha') ;
              $dir= $request->getPostGet('dir') ;
                 $corr= $request->getPostGet('correo') ;
                $data = array($aliasP,$det,$tip,$cant,$fec,$dir,$corr); 
                $modelo = new AnonimoModelo($db); 
                 if($modelo->registrarAnonimo($data)){
                 $respuesta['error']="";
                  $respuesta['ok'] = "Operacion realizada";
              }else{
                  $respuesta['error'] = "Problemas al realizar operacion!!";
              }

		  }


        

		header('Content-Type: application/x-json; charset=utf-8');
        echo(json_encode($respuesta));
		

	}
	//--------------------------------------------------------------------


}
