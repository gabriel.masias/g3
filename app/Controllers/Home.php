<?php

namespace App\Controllers;
use CodeIgniter\Controller;
class Home extends BaseController
{
	public function __construct() {
     	  helper(['form', 'url']);
       	    	            	
    }
	public function index()
	{
		return view('header').view('portada').view('footer');
	}
	//	public function dash()
//	{
	//	return view('tablero');
	//}
//public function ejemplo()
	//{
	//	return view('ejemplo');
	//}
	public function psicologica()
	{
		return view('header').view('psicologico').view('footer');
	}
	public function sexual()
	{
		return view('header').view('sexual').view('footer');
	}
	public function fisica()
	{
		return view('header').view('fisica').view('footer');
	}
	public function economica()
	{
		return view('header').view('economica').view('footer');
	}
	public function desigualdad()
	{
		return view('header').view('desigualdad').view('footer');
	}
	public function patrimonial()
	{
		return view('header').view('patrimonial').view('footer');
	}

	public function registro()
	{
		return view('header').view('registro').view('footer');
	}

	public function login()
	{
		return view('header').view('login');
	}
	public function contacto()
	{
		return view('contacto');
	}
  public function QuienesSomos()
	{
		return view('header').view('QuienesSomos').view('footer');
	}

	public function Listar()
	{
		return view('header').view('ListarAnonimo');
	}


	




public function doSave()
	{
		$validation =  \Config\Services::validation();
		$respuesta = array();
		 $input = $this->validate([


			'docid' => [
				'rules' => 'required|exact_length[8]',
				'errors' =>[
					'required' => 'El campo Doc. Identidad es obligatorio',
					'exact_length' => 'El Doc. Identidad debe ser 8 digitos'
					]
				],

			'nombre' => [
				'rules' => 'required|min_length[2]|max_length[30]
|alpha_space',
				'errors' =>[
					'required' => 'El campo Nombre es obligatorio',
					'min_length' => 'El nombre no debe ser menor de 2 caracteres',
					'max_length' => 'El nombre no debe ser mayor de 30 caracteres',
					'alpha_space' => 'El nombre no debe tener caracteres no letras'
					]
				]	,
					'apel' => [
				'rules' => 'required|min_length[2]|max_length[50]
|alpha_space',
				'errors' =>[
					'required' => 'El campo apellido es obligatorio',
					'min_length' => 'El apellido no debe ser menor de 2 caracteres',
					'max_length' => 'El apellido no debe ser mayor de 50 caracteres',
					'alpha_space' => 'El apellido no debe tener caracteres no letras'
					]
				]	,
					'cel' => [
				'rules' => 'required|min_length[8]|max_length[9]
|numeric',
				'errors' =>[
					'required' => 'El campo numero celular es obligatorio',
					'min_length' => 'El numero celular no debe ser menor de 8 caracteres',
					'max_length' => 'El numero celular no debe ser mayor de 9 caracteres',
					'numeric' => 'El numero celular no debe tener caracteres que no sean digitos'
					]
				]	,
				'correo' => [
				'rules' => 'required|min_length[5]|max_length[30]
|valid_email',
				'errors' =>[
					'required' => 'El campo correo eletronico es obligatorio',
					'min_length' => 'El correo eletronico no debe ser menor de 5 caracteres',
					'max_length' => 'El correo eletronico no debe ser mayor de 30 caracteres',
					'valid_email' => 'El correo eletronico debe respetar el formato de un '
					]
				]	

,
				'dir' => [
				'rules' => 'required|min_length[10]|max_length[200]
|alpha_numeric_punct',
				'errors' =>[
					'required' => 'El campo direccion es obligatorio',
					'min_length' => 'El campo direccion no debe ser menor de 10 caracteres',
					'max_length' => 'El campo direccion no debe ser mayor de 200 caracteres',
					'alpha_numeric_punct' => 'El campo direccion debe respetar caracteres alfa numericos'
					]
				]	

,
			   'det' => [
				'rules' => 'required|min_length[10]|max_length[400]
|alpha_numeric_punct',
				'errors' =>[
					'required' => 'El campo detalles de lo ocurrido es obligatorio',
					'min_length' => 'El campo detalles de lo ocurrido no debe ser menor de 10 caracteres',
					'max_length' => 'El campo detalles de lo ocurrido no debe ser mayor de 400 caracteres',
					'alpha_numeric_punct' => 'El campo detalles de lo ocurrido debe respetar caracteres alfa numericos'
					]
				]	

,
				'tip' => [
				'rules' => 'required|in_list[Psicológico, Sexual, Físico, Económico]',
				'errors' =>[
					'required' => 'El campo tipo de maltrato es obligatorio',
					'in_list' => 'El campo tipo de maltrato debe ser los datos de seleccion'
					]
				]	
,
				'cant' => [
				'rules' => 'required|integer|less_than[31]|greater_than[1]',
				'errors' =>[
					'required' => 'El campo Cantidad de Agresores es obligatorio',
					'integer' => 'El campo Cantidad de Agresores debe ser los datos de seleccion',
					
				   'greater_than' => 'El campo Cantidad de Agresores no debe ser menor a 1 ',
					
					
					]
				]	

,
				   'fecha' => [
					   'rules'=> 'required',
					   'errors' => [
						   'required' => 'La fecha del incidente es obligatoria'
					   ]
				   ]
				   ,

				   'foto' =>[
					   'uploaded[foto]',
					   'mime_in[foto,image/jpg,image/jpeg,image/png,application/pdf,application/octet-stream,video/mp4,audio/mpeg3,audio/mp3]',
					   'max_size[foto,1024]',
					   'errors' =>[
						   'uploaded' => 'No se envio un archivo',
						   'mime_in' => 'No se envio uun formato aceptado(Fotos: .jpg, .png, .jpeg
						   - Videos: .mp4, .avi
						   - Audio: .mp3
						   - Documento: .docx, .pdf)',
						   'max_size' => 'El archivo no debe exceder de 1MB'
					   ]
				   ]
				   ,
			   //Formulario 2


				'dir1' => [
				'rules' => 'required|min_length[10]|max_length[100]
|alpha_numeric_punct',
				'errors' =>[
					'required' => 'El campo direccion es obligatorio',
					'min_length' => 'El campo direccion no debe ser menor de 10 caracteres',
					'max_length' => 'El campo direccion no debe ser mayor de 100 caracteres',
					'alpha_numeric_punct' => 'El campo direccion debe respetar caracteres alfa numericos'
					]
				]	

,
			   'det1' => [
				'rules' => 'required|min_length[10]|max_length[100]
|alpha_numeric_punct',
				'errors' =>[
					'required' => 'El campo detalles de lo ocurrido es obligatorio',
					'min_length' => 'El campo detalles de lo ocurrido no debe ser menor de 10 caracteres',
					'max_length' => 'El campo detalles de lo ocurrido no debe ser mayor de 100 caracteres',
					'alpha_numeric_punct' => 'El campo detalles de lo ocurrido debe respetar caracteres alfa numericos'
					]
				]	

,
				'tip1' => [
				'rules' => 'required|in_list[Psicológico, Sexual, Físico, Económico]',
				'errors' =>[
					'required' => 'El campo tipo de maltrato es obligatorio',
					'in_list' => 'El campo tipo de maltrato debe ser los datos de seleccion'
					]
				]	
,
				'cant1' => [
				'rules' => 'required|integer|less_than[31]|greater_than[1]',
				'errors' =>[
					'required' => 'El campo Cantidad de Agresores es obligatorio',
					'integer' => 'El campo Cantidad de Agresores debe ser los datos de seleccion',
					'less_than' => 'El campo Cantidad de Agresores no debe exceder las 30horas',
				   'greater_than' => 'El campo Cantidad de Agresores no debe ser menor a 2 horas',
					
					
					]
				]	,

				'fecha1' => [
				   'rules'=> 'required',
				   'errors' => [
					   'required' => 'La fecha del incidente es obligatoria'
				   ]
			   ]
			   ,

			   'foto1' =>[
				   'uploaded[foto]',
				   'mime_in[foto,image/jpg,image/jpeg,image/png,application/pdf,application/octet-stream,video/mp4,audio/mpeg3,audio/mp3]',
				   'max_size[foto,1024]',
				   'errors' =>[
					   'uploaded' => 'No se envio un archivo',
					   'mime_in' => 'No se envio uun formato aceptado(Fotos: .jpg, .png, .jpeg
					   - Videos: .mp4, .avi
					   - Audio: .mp3
					   - Documento: .docx, .pdf)',
					   'max_size' => 'El archivo no debe exceder de 1MB'
				   ]
			   ]




		 ]);
		  if (!$input) {
		   $respuesta['error'] = $validation->listErrors();
		  }else{
		   $respuesta['ok']="validacion correcta";
		  }

		  header('Content-Type: application/x-json; charset=utf-8');
        echo(json_encode($respuesta));


	}


}
