<?php namespace App\Models;

use CodeIgniter\Model;

class PrivadoModelo extends Model
{


  public function listar()
  {
    $db = \Config\Database::connect();
    $sql = "CALL sp_ListarDenuncias()";
    $result=$db->query($sql);
    $db->close();
    return $result->getResultArray();   
  }

  public function registrarPersona($dataP)
    {
    	$db = \Config\Database::connect();
    	$sql = "CALL sp_AgregarDenuncia (?,?,?,?,?,?,?,?,?,?,?,@s)";
    	$db->query($sql,$dataP);
    	$res =$db->query('select @s as out_param');
    	$db->close();
    	return   $res->getRow()->out_param;    
    }




	
}
