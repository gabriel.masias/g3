<?php namespace App\Models;

use CodeIgniter\Model;

class ListarAnonimoModelo extends Model
{


  public function ListarAnonimo()
  {
    $db = \Config\Database::connect();
    $sql = "CALL sp_ListarDenunciasAnonimas()";
    $result=$db->query($sql);
    $db->close();
    return $result->getResultArray();   
  }

}  

