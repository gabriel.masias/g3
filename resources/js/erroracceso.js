//body
alert("USTED NO TIENE ACCESO");

anime({
    targets: '.row svg',
    translateY: 10,
    autoplay: true,
    loop: true,
    easing: 'easeInOutSine',
    direction: 'alternate'
  });
  
  anime({
    targets: '#zero',
    translateX: 10,
    autoplay: true,
    loop: true,
    easing: 'easeInOutSine',
    direction: 'alternate',
    scale: [{value: 1}, {value: 1.4}, {value: 1, delay: 250}],
      rotateY: {value: '+=180', delay: 200},
  });
  
  
//BLOQUEO DE TECLAS

 //BLOQUEAR CLICK DERECHO
 document.addEventListener('contextmenu', event => event.preventDefault());
 //BLOQUEAR f12
 $(document).keydown(function (event) {
    if (event.keyCode == 123) { // Prevent F12
        return false;
    } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
        return false;
    }
});

 //BLOQUEAR CONTROL + U Y CONTROL +S

onkeydown = e => {
	let tecla = e.which || e.keyCode;
	
	// Evaluar si se ha presionado la tecla Ctrl:
	if ( e.ctrlKey ) {
	  // Evitar el comportamiento por defecto del nevagador:
	  e.preventDefault();
	  e.stopPropagation();
	  
	  // Mostrar el resultado de la combinación de las teclas:
	  if ( tecla === 85 )
		console.log("Ha presionado las teclas Ctrl + U");
	  
	  if ( tecla === 83 )
		console.log("Ha presionado las teclas Ctrl + S");
	}
  }
