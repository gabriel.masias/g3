function doaction(){ 
	//body
	alert("Bienvenidos..... GRUPO 03 | REGISTRA TU DENUNCIA ANONIMA");
	//Ocultar la ventana de error y la ventana de succ
	$("#error").hide();
  	$("#succ").hide();
  	$("#paneltabla2").hide();
	 //Evitar que la pagina se refresque, se refresca solo el formulario
  $('#frmreg').submit(function() {
  	$("#error").hide();
	  $("#succ").hide();

	  var formData= new FormData($('#frmreg')[0]);
  	$.ajax({
		  //ruta de registro
  		url:  ruta+"/Anonimo/doSave",
  		type: 'post',
  		dataType: 'json',
  		data: formData,
		  //omitir 
  		cache: false,
        contentType: false,
        processData: false,  
  		success: function(e){  	
			  //las alertas		
  			if(e.error==""){
  				$("#succ").show();
				$("#mensaje_ok").html(e.ok);   
				 $('#frmreg')[0].reset();
				 $("#succ").hide(6000);  
  			}else{
  				$("#error").show();
  				$("#mensaje_error").html(e.error);
  			}
  		}
  	})
  	.done(function() {
  		console.log("success");
  	})
  	.fail(function() {
  		console.log("error");
  	})
  	.always(function() {
  		console.log("complete");
  	});
  	  return false;
  	
  });

$("#btn_mostrar").click(function() {
	  	$("#paneltabla2").hide();
	$.ajax({
		url: ruta+"/Anonimo/doList",
		type: 'post',
		dataType: 'json',
		data: {},
		success: function(e){  
			 if (e.data.length>0){
			 		$("#paneltabla2").show();
			 	$(function () {
					     $('#table').bootstrapTable({
					        data: e.data
					      });
				$('#table').bootstrapTable('hideColumn', 'v1');	     
			 	  });
			}
			 else{
			 	alert("No hay Registros");
			 }	
		}
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
});

 //BLOQUEO DE TECLAS

 //BLOQUEAR CLICK DERECHO
 document.addEventListener('contextmenu', event => event.preventDefault());
 //BLOQUEAR f12
 $(document).keydown(function (event) {
    if (event.keyCode == 123) { // Prevent F12
        return false;
    } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
        return false;
    }
});

 //BLOQUEAR CONTROL + U Y CONTROL +S

onkeydown = e => {
	let tecla = e.which || e.keyCode;
	
	// Evaluar si se ha presionado la tecla Ctrl:
	if ( e.ctrlKey ) {
	  // Evitar el comportamiento por defecto del nevagador:
	  e.preventDefault();
	  e.stopPropagation();
	  
	  // Mostrar el resultado de la combinación de las teclas:
	  if ( tecla === 85 )
		console.log("Ha presionado las teclas Ctrl + U");
		
	  
	  if ( tecla === 83 )
		console.log("Ha presionado las teclas Ctrl + S");
	}
  }







}

